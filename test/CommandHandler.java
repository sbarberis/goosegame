import com.goose.cmdline.CommandInterpreter;
import com.goose.gamelogic.GameLogic;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by sbarberis on 21/05/2019.
 */


public class CommandHandler {

    @Test
    public void addPlayerWithoutPlayerName(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        String response = commandInterpreter.readCommand("add player");
        assertEquals("", response);
    }

    @Test
    public void addPlayerWithPlayerName(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        String response = commandInterpreter.readCommand("add player Stefano");
        assertEquals("players: Stefano", response);
    }

    @Test
    public void addPlayersToTheGame(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Mario");
        String response = commandInterpreter.readCommand("add player Luigi");

        assertEquals("players: Mario,Luigi", response);
    }

    @Test
    public void movePlayerFromStartTo4(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Stefano");
        String response = commandInterpreter.readCommand("move Stefano 2,2");
        assertEquals("Stefano rolls 2, 2. Stefano moves from Start to 4", response);
    }

    @Test
    public void movePlayerToTheBridge(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Peach");
        String response = commandInterpreter.readCommand("move Peach 3,3");
        assertEquals("Peach rolls 3, 3. Peach moves from Start to The Bridge. Peach jumps to 12", response);
    }

    @Test
    public void movePlayerToTheGoose(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Bowser");
        String response = commandInterpreter.readCommand("move Bowser 2,3");
        assertEquals("Bowser rolls 2, 3. Bowser moves from Start to 5, The Goose. Bowser moves again and goes to 10", response);
    }

    @Test
    public void playerWinTheMatch(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Toad");
        String response = "";
        for (int i=1;i<=4;i++){
            response = commandInterpreter.readCommand("move Toad 6,3");

        }
        assertEquals("Toad rolls 6, 3. Toad moves from 54 to 63. And the winner is Toad", response);

    }

    @Test
    public void moveBounceToTheGoose(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Boo");

        String response = commandInterpreter.readCommand("move Boo 6,3");

        commandInterpreter.readCommand("move Boo 6,6");
        commandInterpreter.readCommand("move Boo 6,6");
        response = commandInterpreter.readCommand("move Boo 6,6");


        assertEquals("Boo rolls 6, 6. Boo moves from 60 to 63. Boo bounces! Boo returns to 54", response);
    }

    @Test
    public void prankAnotherPlayer(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Ryu");
        commandInterpreter.readCommand("add player Ken");

        commandInterpreter.readCommand("move Ryu 3,2");
        String response = commandInterpreter.readCommand("move Ken 3,2");

        assertEquals("Ken rolls 3, 2. Ken moves from Start to 10. On 10 there is Ryu, who returns to 0", response);
    }

    @Test
    public void checkAlreadyExistingUser(){
        CommandInterpreter commandInterpreter = getCommandInterpreter();
        commandInterpreter.readCommand("add player Stego");
        String response = commandInterpreter.readCommand("add player Stego");
        assertEquals("Stego already existing player", response);
    }

    private CommandInterpreter getCommandInterpreter(){
        GameLogic.resetGame();
        CommandInterpreter commandInterpreter = new CommandInterpreter();
        commandInterpreter.initCommands();
        return  commandInterpreter;
    }
}
