The Goose Game

Program starts from Main class and prompt for a commad

************
* Commands *
************

*** Add a new player to the game:

add player <player_name>

e.g.
add player Stefano


*** Move player with predefined dice

move <player_name> <dice1>,<dice2>

e.g.
move Stefano 3,2

*** Move player with random dice roll

move <player_name>

e.g.
move Stefano

When one of the players reach the number 63 the game end and all the players are removed from the game.
