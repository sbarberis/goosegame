package com.goose.cmdline.events;

import com.goose.gamelogic.GameLogic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sbarberis on 21/05/2019.
 */
public class CommandHandler implements Handler {

    public String  undefined(String command){
        return "";
    }

    public String sendAddPlayerCommand(String command) throws CommandSyntaxException {
        String response = "";
        String playerName = "";

        String addPlayerPattern = "add player ([a-zA-Z]+)";

        Pattern pattern = Pattern.compile(addPlayerPattern);
        Matcher matcher = pattern.matcher(command);

        if(matcher.matches()){
            playerName = matcher.group(1);
        }else{
            throw new CommandSyntaxException("Syntax error");
        }

        if(GameLogic.isUnique(playerName)){
            GameLogic.addPlayer(playerName);
            response = String.format("players: %s", GameLogic.getPlayerList().toString());
        }else{
            response = String.format("%s already existing player", playerName);
        }

        return response;
    }

    public String sendMoveCommand(String command) throws CommandSyntaxException {
        int rollOne = 0;
        int rollTwo = 0;
        String playerName = "";
        String response = "";
        String movePlayerPatternWithParams = "move ([a-zA-Z]+) ([0-9]+),\\s*([0-9]+)";
        String movePlayerPatternRandom = "move ([a-zA-Z]+)";


        Pattern pattern = Pattern.compile(movePlayerPatternWithParams);
        Matcher matcher = pattern.matcher(command);
        if(matcher.matches()){
            playerName = matcher.group(1);
            try{
                rollOne = Integer.parseInt(matcher.group(2));
                rollTwo = Integer.parseInt(matcher.group(3));

                response = GameLogic.executePlayerRoll(playerName,rollOne,rollTwo);

            }catch (NumberFormatException e){
                throw new CommandSyntaxException("Syntax error");
            }
        }

        pattern = Pattern.compile(movePlayerPatternRandom);
        matcher = pattern.matcher(command);
        if(matcher.matches()){
            playerName = matcher.group(1);
            response = GameLogic.executePlayerRoll(playerName,null,null);
            if(response == null){
                throw new CommandSyntaxException(String.format("Player %s not found in player list", playerName));
            }
        }

        return response;
    }


    public void sendQuitCommand(String command){
        System.out.println("Goose Bye");
        System.exit(0);
    }
}
