package com.goose.cmdline.events;

/**
 * Created by sbarberis on 22/05/2019.
 */
public class CommandSyntaxException extends Exception {

    public CommandSyntaxException(String message){
        super(message);
    }
}
