package com.goose.cmdline.events;

/**
 * Created by sbarberis on 21/05/2019.
 */
public interface Handler {
    String sendAddPlayerCommand(String command) throws CommandSyntaxException;
    String sendMoveCommand(String command) throws CommandSyntaxException;
    String undefined(String command) throws CommandSyntaxException;
    void sendQuitCommand(String command) throws CommandSyntaxException;
}
