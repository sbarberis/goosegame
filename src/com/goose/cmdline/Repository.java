package com.goose.cmdline;

import java.util.Hashtable;

/**
 * Created by sbarberis on 20/05/2019.
 */
public interface Repository {

    Hashtable<String,String> initCommands();
}
