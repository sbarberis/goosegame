package com.goose.cmdline;

import com.goose.cmdline.events.CommandHandler;
import com.goose.cmdline.events.Handler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;

/**
 * Created by sbarberis on 20/05/2019.
 */
public class CommandInterpreter {
    private Hashtable<String,String> gooseCommands = new Hashtable<>();
    private Repository repository = new CommandsRepo();
    private Handler handler = new CommandHandler();

    public void initCommands(){
        gooseCommands = repository.initCommands();
    }

    public String readCommand(String command){
        String commandName = command.split(" ")[0];
        String event = gooseCommands.get(commandName);
        event = event != null ? event:"undefined";
        String response = invokeCommandMethod(event,command);
        return response;
    }

    private String invokeCommandMethod(String methodName, String command){
        String response = "";
        try{
            Method method =  handler.getClass().getDeclaredMethod(methodName,String.class);
            method.setAccessible(true);
            response = (String)method.invoke(handler,command);
        }catch (Exception e){
            sendFailureMessage(((InvocationTargetException) e).getTargetException().getMessage());
        }
        return response;
    }

    private void sendFailureMessage(String message){
        System.out.println(message);
    }
}
