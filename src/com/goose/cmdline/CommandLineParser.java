package com.goose.cmdline;

import java.util.Scanner;

/**
 * Created by sbarberis on 20/05/2019.
 */
public class CommandLineParser {
    public static void parse(){
        CommandInterpreter commandInterpreter = new CommandInterpreter();
        commandInterpreter.initCommands();
        while(true){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please select Command:");
            String command = scanner.nextLine();
            String response = commandInterpreter.readCommand(command);
            System.out.println(response);
        }
    }
}
