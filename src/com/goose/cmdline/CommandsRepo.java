package com.goose.cmdline;

import java.lang.reflect.Field;
import java.util.Hashtable;

/**
 * Created by sbarberis on 20/05/2019.
 */
public class CommandsRepo implements Repository {

    @Command(pattern = "quit,sendQuitCommand")
    private String cmdQuit;

    @Command(pattern = "move,sendMoveCommand")
    private String cmdMove;

    @Command(pattern = "add,sendAddPlayerCommand")
    private String cmdAddPlayer;


    public Hashtable<String,String> initCommands(){
        Hashtable<String,String> gooseCommands = new Hashtable<>();

        Field[] fields =  this.getClass().getDeclaredFields();
        for(Field f:fields){
            if(f.isAnnotationPresent(Command.class)){
                f.setAccessible(true);
                try{
                    String[] command =  f.getAnnotation(Command.class).pattern().split(",");
                    gooseCommands.put(command[0],command[1]);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        }
        return gooseCommands;
    }
}
