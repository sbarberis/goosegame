package com.goose;

import com.goose.cmdline.CommandLineParser;

public class Main {

    public static void main(String[] args) {
        CommandLineParser.parse();
    }
}
