package com.goose.gamelogic;

import com.goose.gamelogic.entities.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sbarberis on 20/05/2019.
 */
public class GameLogic {
    private static  List<Player> players = new ArrayList<>();
    private static String gooseCells = "|5|9|14|18|23|27|";
    private static Integer theBridge = 6;
    private static int winCell = 63;
    private static int currentTurn = 0;

    public static void resetGame(){
        players.clear();
    }

    public static Boolean isUnique(String playerName){
        Player player = getCurrentPLayer(playerName);
        return player == null;
    }

    public static void addPlayer(String playeName){
        players.add(new Player(playeName));
    }

    public static String getPlayerList(){
        return players.stream().map(p->p.getName()).collect(Collectors.joining(","));
    }


    public static String executePlayerRoll(String playerName, Integer _rollOne, Integer _rollTwo){
        String response = "";
        Player player = getCurrentPLayer(playerName);
        if(player == null){
            return null;
        }

        if (!player.getName().equals(players.get(currentTurn).getName())){
            response = "Not your turn";
            return response;
        }

        int rollOne = (_rollOne != null)?_rollOne:player.getDiceRoll();
        int rollTwo = (_rollTwo != null )?_rollTwo:player.getDiceRoll();

        rollOne = (rollOne >=1 && rollOne <= 6)?rollOne:1;
        rollTwo = (rollTwo >=1 && rollTwo <= 6)?rollTwo:1;

        int playerNewPosition = rollOne + rollTwo + player.getBoardPosition();


        String currentPosition = player.getBoardPosition() == 0 ? "Start": Integer.toString(player.getBoardPosition());

        if (playerNewPosition == winCell){
            response = String.format("%s rolls %d, %d. %s moves from %s to %d. And the winner is %s",
                    playerName, rollOne,rollTwo,playerName,currentPosition,playerNewPosition, playerName
            );
            player.setBoardPosition(playerNewPosition);
            GameLogic.resetGame();
            return response;
        }

        if(playerNewPosition > winCell){
            playerNewPosition = winCell - (playerNewPosition-winCell);
            response = String.format("%s rolls %d, %d. %s moves from %s to %d. %s bounces! %s returns to %d",
                    playerName,rollOne,rollTwo, playerName,currentPosition,winCell,playerName,playerName,playerNewPosition
                    );
            player.setBoardPosition(playerNewPosition);
            return response;
        }

        response = String.format("%s rolls %d, %d. %s moves from %s to %d",playerName,rollOne,rollTwo,playerName,currentPosition,playerNewPosition);

        if(playerNewPosition == theBridge){
            playerNewPosition = 12;
            response = String.format("%s rolls %d, %d. %s moves from %s to The Bridge. %s jumps to %d",
                    playerName,rollOne,rollTwo,playerName,currentPosition,playerName,playerNewPosition
            );
        }

        int playerNextPosition = playerNewPosition;
        String gooseMessage = "";

        while (true){
            if(gooseCells.contains(String.format("|%s|",Integer.toString(playerNextPosition)))){
                playerNextPosition += rollOne + rollTwo;
                if("".equals(gooseMessage)){
                    gooseMessage = String.format("%s rolls %d, %d. %s moves from %s to %d, The Goose. %s moves again and goes to %d",
                            playerName,rollOne,rollTwo,playerName,currentPosition,playerNewPosition,playerName,playerNextPosition
                    );
                }else{
                    gooseMessage += String.format(", The Goose. %s moves again and goes to %d", playerName, playerNextPosition);
                }
            }else{
                playerNewPosition = playerNextPosition;
                if (!"".equals(gooseMessage)) {
                    response = gooseMessage;
                }
                break;
            }
        }

        Player othePlayer = searchPlayerByPosition(playerNewPosition);

        if(othePlayer != null){
            othePlayer.setBoardPosition(player.getBoardPosition());
            response = String.format("%s rolls %d, %d. %s moves from %s to %d. On %d there is %s, who returns to %d",
                    playerName,rollOne,rollTwo,playerName,currentPosition,playerNewPosition,playerNewPosition,othePlayer.getName(),player.getBoardPosition()
            );
        }

        player.setBoardPosition(playerNewPosition);

        currentTurn++;
        if(currentTurn > players.size()-1){
            currentTurn = 0;
        }

        return response;
    }

    private static Player searchPlayerByPosition(int position){
        if(position == 0){
            return null;
        }
        Player player = players.stream().filter(p->p.getBoardPosition() == position).findFirst().orElse(null);
        return player;
    }

    private static  Player getCurrentPLayer(String playerName){
        Player player = players.stream().filter(p->playerName.toLowerCase().equals(p.getName().toLowerCase())).findAny().orElse(null);
        return player;
    }

}
