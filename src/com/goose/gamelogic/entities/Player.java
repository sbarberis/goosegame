package com.goose.gamelogic.entities;

/**
 * Created by sbarberis on 20/05/2019.
 */
public class Player {
    private String name;
    private Roll roll = new Roll();
    private int boardPosition;

    public int getBoardPosition(){
        return boardPosition;
    }

    public void setBoardPosition(int boardPosition){
        this.boardPosition = boardPosition;
    }

    public Player(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getDiceRoll(){
        Dice dice = roll.diceRoll();
        return dice.getRollValue();
    }
}
