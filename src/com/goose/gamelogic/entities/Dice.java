package com.goose.gamelogic.entities;

/**
 * Created by sbarberis on 21/05/2019.
 */
public class Dice {
    private int rollValue;
    public void setRollValue(int number){
        this.rollValue = number;
    }
    public int getRollValue(){
        return rollValue;
    }
}
