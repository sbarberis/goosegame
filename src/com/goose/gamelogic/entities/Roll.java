package com.goose.gamelogic.entities;

import com.goose.gamelogic.entities.Dice;

import java.util.Random;

/**
 * Created by sbarberis on 21/05/2019.
 */
public class Roll {

    public Dice diceRoll(){
        Dice dice = new Dice();
        dice.setRollValue(generateRandomRoll());
        return  dice;
    }


    private int generateRandomRoll(){
        Random randomRoll = new Random();
        return randomRoll.nextInt(6) + 1;
    }
}
